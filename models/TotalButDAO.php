<?php

require_once(PATH_MODELS.'DAO.php');

class TotalBut extends DAO
{
    function getSup($nbBut){
        $req = 'select "m"."idMatch", "m"."dateMatch", "m"."idEquipeDomicileMatch", "s"."butDomicileScore", "s"."butExterieurScore","m"."idEquipeExterieurMatch", ("butDomicileScore" + "butExterieurScore") as totalBut
        from "Match" "m" 
        INNER JOIN "Score" "s" 
            on "m"."idMatch" = "s"."idMatch"
        INNER JOIN "Equipe" "e" 
            on "e"."idEquipe" = "m"."idEquipeDomicileMatch" OR  "e"."idEquipe" = "m"."idEquipeExterieurMatch"
        WHERE ("butDomicileScore" + "butExterieurScore") > ?
        group by "m"."idMatch", "s"."butDomicileScore", "s"."butExterieurScore";';
        $resultat = $this->queryAll($req, array($nbBut));
        if($resultat)
        {
        $listeScore=array();
        foreach ($resultat as $score) 
        {
            array_push($listeScore, array(
                    "idMatch" => $score["idMatch"],
                    "dateMatch" => $score["dateMatch"],
                    "idEquipeDomicileMatch" => $score["idEquipeDomicileMatch"],
                    "butDomicileScore" => $score["butDomicileScore"],
                    "butExterieurScore" => $score["butExterieurScore"],
                    "idEquipeExterieurMatch" => $score["idEquipeExterieurMatch"],
                    "totalBut" =>  $score["totalbut"],
                ));
        }
        return $listeScore;
        }
        
        else return null; 
    }

    function getInf($nbBut){
        $req = 'select "m"."idMatch", "m"."dateMatch", "m"."idEquipeDomicileMatch", "s"."butDomicileScore", "s"."butExterieurScore","m"."idEquipeExterieurMatch", ("butDomicileScore" + "butExterieurScore") as totalBut
        from "Match" "m" 
        INNER JOIN "Score" "s" 
            on "m"."idMatch" = "s"."idMatch"
        INNER JOIN "Equipe" "e" 
            on "e"."idEquipe" = "m"."idEquipeDomicileMatch" OR  "e"."idEquipe" = "m"."idEquipeExterieurMatch"
        WHERE ("butDomicileScore" + "butExterieurScore") < ?
        group by "m"."idMatch", "s"."butDomicileScore", "s"."butExterieurScore";';
        $resultat = $this->queryAll($req, array($nbBut));
        if($resultat)
        {
        $listeScore=array();
        foreach ($resultat as $score) 
        {
            array_push($listeScore, array(
                    "idMatch" => $score["idMatch"],
                    "dateMatch" => $score["dateMatch"],
                    "idEquipeDomicileMatch" => $score["idEquipeDomicileMatch"],
                    "butDomicileScore" => $score["butDomicileScore"],
                    "butExterieurScore" => $score["butExterieurScore"],
                    "idEquipeExterieurMatch" => $score["idEquipeExterieurMatch"],
                    "totalBut" =>  $score["totalbut"],
                ));
        }
        return $listeScore;
        }
        
        else return null; 
    }
}