<?php
require_once(PATH_ENTITY.'StatsJoueur.php');
require_once(PATH_MODELS.'DAO.php');

class StatsJoueurDAO extends DAO
{
	//récupère les stats de tous les joueurs
  public function getAllStatsJoueur(){
    $req = 'SELECT * FROM "StatsJoueur"';
    $resultat = $this->queryAll($req);
    if($resultat)
    {
      $listeStatsJoueur=array();
      foreach ($resultat as $joueur) 
      {
        $listeStatsJoueur[] =  new StatsJoueur ($joueur["matchJoueJoueur"],$joueur["butMarqueJoueur"],$joueur["passeDecisiveJoueur"],$joueur["idJoueur"],$joueur["idCompetition"]);
      }
      return $listeStatsJoueur;
    }
    
    else return null;     
	
  }

  //récupère les stats d'un joueur en fonction de son ID
  public function getIdStatJoueur($idJoueur){
    $req = 'SELECT * FROM "StatsJoueur" WHERE "idJoueur" = ? ';
    $resultat = $this->queryAll($req, array($idJoueur));
    if($resultat)
    {
      $listeStatsJoueur=array();
      foreach ($resultat as $joueur) 
      {
        $listeStatsJoueur[] = new StatsJoueur ($joueur["matchJoueJoueur"],$joueur["butMarqueJoueur"],$joueur["passeDecisiveJoueur"],$joueur["idJoueur"],$joueur["idCompetition"]);
      }
      return $listeStatsJoueur;
    }
    else return null;     
  }

}