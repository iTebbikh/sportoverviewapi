<?php

// Implémente le pattern Singleton
class Connection
{
  private $_bdd = null;
  private static $_instance = null;

  //appelée par new
  private function __construct ()
  {
	  $this->_bdd = new PDO('pgsql:host='.BD_HOST.'; dbname='.BD_DBNAME.';', BD_USER, BD_PWD);
    $this->_bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  
  }

  //appelée par clone
  private function __clone()
  {
  }

  //appelée par unserialize
  private function __wakeup()
  {
  }

  public static function getInstance()
  {
    if(is_null(self::$_instance))
      self::$_instance = new Connection();
    return self::$_instance;
  }

  public function getBdd()
  { 
    return $this->_bdd;
  }
}


