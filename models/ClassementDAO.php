<?php
require_once(PATH_ENTITY.'Classement.php');
require_once(PATH_MODELS.'DAO.php');

class ClassementDAO extends DAO
{
    //récupère le classement d'une équipe en fonction de son ID
    public function getClassementId($idEquipe){
        $req = 'SELECT * FROM "Classement" WHERE "idEquipe" = ? ';
        $resultat = $this->queryAll($req, array($idEquipe));
        if($resultat)
        {
            $listeClassement= array();
            foreach ($resultat as $Classement) 
            {
                $listeClassement[] = new Classement ($Classement["idEquipe"],$Classement["idCompetition"],$Classement["phaseCompetition"],$Classement["matchJoueEquipe"],$Classement["matchGagneEquipe"],$Classement["matchNulEquipe"],$Classement["matchPerduEquipe"],$Classement["butMarqueEquipe"],$Classement["butEncaisseEquipe"],$Classement["differenceButEquipe"],$Classement["pointEquipe"]);
            }
            return $listeClassement;
        }
        else return null;     
    }

}