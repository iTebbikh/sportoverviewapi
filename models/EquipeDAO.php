<?php
require_once(PATH_ENTITY.'Equipe.php');
require_once(PATH_MODELS.'DAO.php');

class EquipeDAO extends DAO
{
	//récupère toutes les équipes
  public function getAllEquipe(){
    $req = 'SELECT * FROM "Equipe"';
    $resultat = $this->queryAll($req);
    if($resultat)
    {
      $listeEquipe = array();
      foreach ($resultat as $equipe) 
      {
        $listeEquipe[] =  new Equipe ($equipe["idEquipe"],$equipe["nomEquipe"],$equipe["paysEquipe"]);
      }
      return $listeEquipe;
    }
    
    else return null;     
	
  }

  //récupère une équipe en fonction de son ID
  public function getEquipeId($idEquipe){
    $req = 'SELECT * FROM "Equipe" WHERE "idEquipe" = ? ';
    $resultat = $this->queryAll($req, array($idEquipe));
    if($resultat)
    {
      $listeEquipe= array();
      foreach ($resultat as $equipe) 
      {
        $listeEquipe[] = new Equipe ($equipe["idEquipe"],$equipe["nomEquipe"],$equipe["paysEquipe"]);
      }
      return $listeEquipe;
    }
    else return null;     
  }

}