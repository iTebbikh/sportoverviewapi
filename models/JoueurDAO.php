<?php
require_once(PATH_ENTITY.'Joueur.php');
require_once(PATH_MODELS.'DAO.php');

class JoueurDAO extends DAO
{
    //récupère tous les joueurs
    public function getAllJoueur(){
        $req = 'SELECT * FROM "Joueur"';
        $resultat = $this->queryAll($req);
        if($resultat)
        {
            $listeJoueurs=array();
            foreach ($resultat as $joueur) 
            {
                $listeJoueurs[] =  new Joueur ($joueur["idJoueur"],$joueur["nomJoueur"],$joueur["prenomJoueur"],$joueur["nationaliteJoueur"],$joueur["idEquipe"]);
            }
            return $listeJoueurs;
        }
        else return null;
	
    }

    //récupère un joueur en fonction de son ID
    public function getJoueurId($idJoueur){
        $req = 'SELECT * FROM "Joueur" WHERE "idJoueur" = ? ';
        $resultat = $this->queryAll($req, array($idJoueur));
        if($resultat)
        {
            $listeJoueurs=array();
            foreach ($resultat as $joueur) 
            {
                $listeJoueurs[] = new Joueur ($joueur["idJoueur"],$joueur["nomJoueur"],$joueur["prenomJoueur"],$joueur["nationaliteJoueur"],$joueur["idEquipe"]);
            }
            return $listeJoueurs;
        }
        else return null;     
    }

}