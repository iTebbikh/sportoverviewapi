<?php
require_once(PATH_ENTITY.'Competition.php');
require_once(PATH_MODELS.'DAO.php');

class CompetitionDAO extends DAO
{
	//récupère toutes les compétitions
  public function getAllCompetition(){
    $req = 'SELECT * FROM "Competition"';
    $resultat = $this->queryAll($req);
    if($resultat)
    {
      $listeCompetitions= array();
      foreach ($resultat as $competition) 
      {
        $listeCompetitions[] =  new Competition ($competition["idCompetition"],$competition["nomCompetition"],$competition["saisonCompetition"],$competition["paysCompetition"]);
      }
      return $listeCompetitions;
    }
    
    else return null;     
	
  }

  //récupère une compétition en fonction de son ID
  public function getCompetitionId($idCompetition){
    $req = 'SELECT * FROM "Competition" WHERE "idCompetition" = ? ';
    $resultat = $this->queryAll($req, array($idCompetition));
    if($resultat)
    {
      $listeCompetitions=array();
      foreach ($resultat as $competition) 
      {
        $listeCompetitions[] = new Competition ($competition["idCompetition"],$competition["nomCompetition"],$competition["saisonCompetition"],$competition["paysCompetition"]);
      }
      return $listeCompetitions;
    }
    else return null;     
  }

}