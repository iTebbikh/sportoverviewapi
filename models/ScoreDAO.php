<?php
require_once(PATH_ENTITY.'Score.php');
require_once(PATH_MODELS.'DAO.php');

class ScoreDAO extends DAO
{
	//récupère tous les scores
  public function getAllScore(){
    $req = 'SELECT * FROM "Score"';
    $resultat = $this->queryAll($req);
    if($resultat)
    {
      $listeScore=array();
      foreach ($resultat as $score) 
      {
        $listeScore[] =  new Score ($score["idScore"],$score["butDomicileScore"],$score["butExterieurScore"],$score["idMatch"]);
      }
      return $listeScore;
    }
    
    else return null;     
	
  }

  //récupère un score en fonction de son ID
  public function getScoreId($idScore){
    $req = 'SELECT * FROM "Score" WHERE "idScore" = ? ';
    $resultat = $this->queryAll($req, array($idScore));
    if($resultat)
    {
      $listeScore=array();
      foreach ($resultat as $score) 
      {
        $listeScore[] = new Score ($score["idScore"],$score["butDomicileScore"],$score["butExterieurScore"],$score["idMatch"]);
      }
      return $listeScore;
    }
    else return null;     
  }

}