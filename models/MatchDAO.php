<?php
require_once(PATH_ENTITY.'Match.php');
require_once(PATH_MODELS.'DAO.php');

class MatchDAO extends DAO
{
	//récupère tous les matchs
    public function getAllMatch(){
        $req = 'SELECT * FROM "Match"';
        $resultat = $this->queryAll($req);
        if($resultat)
        {
            $listeMatch=array();
            foreach ($resultat as $match) 
            {
                $listeMatch[] =  new Match ($match["idMatch"],$match["dateMatch"],$match["idEquipeDomicileMatch"],$match["idEquipeExterieurMatch"],$match["roundMatch"],$match["idCompetition"]);
            }
            return $listeMatch;
        }
        else return null;     
	
    }

    //récupère un match en fonction de son ID
    public function getMatchId($idMatch){
        $req = 'SELECT * FROM "Match" WHERE "idMatch" = ? ';
        $resultat = $this->queryAll($req, array($idMatch));
        if($resultat)
        {
            $listeMatch=array();
            foreach ($resultat as $match) 
            {
                $listeMatch[] = new Match ($match["idMatch"],$match["dateMatch"],$match["idEquipeDomicileMatch"],$match["idEquipeExterieurMatch"],$match["roundMatch"],$match["idCompetition"]);
            }
            return $listeMatch;
        }
        else return null;     
    }

    //récupère un match en fonction de l'ID de l'équipe à domicile
    public function getMatchIdEquipeDomicile($idEquipeDomicileMatch){
        $req = 'SELECT * FROM "Match" WHERE "idEquipeDomicileMatch" = ? ';
        $resultat = $this->queryAll($req, array($idEquipeDomicileMatch));
        if($resultat)
        {
            $listeMatch=array();
            foreach ($resultat as $match) 
            {
                $listeMatch[] = new Match ($match["idMatch"],$match["dateMatch"],$match["idEquipeDomicileMatch"],$match["idEquipeExterieurMatch"],$match["roundMatch"],$match["idCompetition"]);
            }
            return $listeMatch;
        }
        else return null;     
    }

    //récupère un match en fonction de l'ID de l'équipe à l'extérieur
    public function getMatchIdEquipeExterieur($idEquipeExterieurMatch){
        $req = 'SELECT * FROM "Match" WHERE "idEquipeExterieurMatch" = ? ';
        $resultat = $this->queryAll($req, array($idEquipeExterieurMatch));
        if($resultat)
        {
            $listeMatch=array();
            foreach ($resultat as $match) 
            {
                $listeMatch[] = new Match ($match["idMatch"],$match["dateMatch"],$match["idEquipeDomicileMatch"],$match["idEquipeExterieurMatch"],$match["roundMatch"],$match["idCompetition"]);
            }
            return $listeMatch;
        }
        else return null;     
    }

    //récupère un match en fonction du round
    public function getMatchRound($roundMatch){
        $req = 'SELECT * FROM "Match" WHERE "roundMatch" = ? ';
        $resultat = $this->queryAll($req, array($roundMatch));
        if($resultat)
        {
            $listeMatch=array();
            foreach ($resultat as $match) 
            {
                $listeMatch[] = new Match ($match["idMatch"],$match["dateMatch"],$match["idEquipeDomicileMatch"],$match["idEquipeExterieurMatch"],$match["roundMatch"],$match["idCompetition"]);
            }
            return $listeMatch;
        }
        else return null;     
    }

    //récupère un match en fonction de l'ID de la compétition
    public function getMatchIdCompetition($idCompetition){
        $req = 'SELECT * FROM "Match" WHERE "idCompetition" = ? ';
        $resultat = $this->queryAll($req, array($idCompetition));
        if($resultat)
        {
            $listeMatch=array();
            foreach ($resultat as $match) 
            {
                $listeMatch[] = new Match ($match["idMatch"],$match["dateMatch"],$match["idEquipeDomicileMatch"],$match["idEquipeExterieurMatch"],$match["roundMatch"],$match["idCompetition"]);
            }
            return $listeMatch;
        }
        else return null;     
    }

    //récupère le dernier match d'une compétition
    public function getLastMatchIdCompetition($idCompetition){
        $req = 'SELECT * FROM "Match" WHERE "idCompetition" = ? AND "dateMatch" < now() order by "dateMatch" desc limit 1';
        $resultat = $this->queryAll($req, array($idCompetition));
        if($resultat)
        {
            $listeMatch=array();
            foreach ($resultat as $match) 
            {
                $listeMatch[] = new Match ($match["idMatch"],$match["dateMatch"],$match["idEquipeDomicileMatch"],$match["idEquipeExterieurMatch"],$match["roundMatch"],$match["idCompetition"]);
            }
            return $listeMatch;
        }
        else return null;     
    }

}