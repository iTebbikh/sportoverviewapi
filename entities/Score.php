<?php

class Score implements JsonSerializable
{
    //variables locales
    private $_idScore;
    private $_butDomicileScore;
    private $_butExterieurScore;
    private $_idMatch;

    //constructeurs
    public function __construct($idScore, $butDomicileScore, $butExterieurScore, $idMatch){
        $this->_idScore =$idScore;
        $this->_butDomicileScore = $butDomicileScore;
        $this->_butExterieurScore = $butExterieurScore;
        $this->_idMatch = $idMatch;
    }

    //getteurs
    public function getIdScore(){
        return $this->_idScore; 
    }
    public function getButDomicileScore(){
        return $this->_butDomicileScore;
    }
    public function getButExterieurScore(){
        return $this->_butExterieurScore;
    }
    public function getIdMatch(){
        return $this->_idMatch; 
    }

    //setteurs
    public function setIdScore($idScore){
        $this->_idScore = $idScore;
    }
    public function setButDomicileScore($butDomicileScore){
        $this->_butDomicileScore = $butDomicileScore;
    }
    public function setButExterieurScore($butExterieurScore){
        $this->_butExterieurScore = $butExterieurScore;
    }
    public function setIdMatch($idMatch){
        $this->_idMatch = $idMatch;
    }

    public function jsonSerialize()
    {
        return [
            'idScore' => $this -> _idScore,
            'butDomicileScore' => $this -> _butDomicileScore,
            'butExterieurScore' => $this -> _butExterieurScore,
            'idMatch' => $this -> _idMatch
        ];
    }

}