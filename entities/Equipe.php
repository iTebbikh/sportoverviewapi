<?php

class Equipe implements JsonSerializable 
{
    //variables locales
    private $_idEquipe;
    private $_nomEquipe;
    private $_paysEquipe;

    //constructeurs
    public function __construct($idEquipe, $nomEquipe, $paysEquipe){
        $this->_idEquipe =$idEquipe;
        $this->_nomEquipe = $nomEquipe;
        $this->_paysEquipe = $paysEquipe;
    }

    //getteurs
    public function getIdEquipe(){
        return $this->_idEquipe; 
    }
    public function getNomEquipe(){
        return $this->_nomEquipe;
    }
    public function getPaysEquipe(){
        return $this->_paysEquipe;
    }

    //setteurs
    public function setIdEquipe($idEquipe){
        $this->_idEquipe = $idEquipe;
    }
    public function setNomEquipe($nomEquipe){
        $this->_nomEquipe = $nomEquipe;
    }
    public function setPaysEquipe($paysEquipe){
        $this->_paysEquipe = $paysEquipe;
    }
    public function jsonSerialize()
    {
        return [
            'id' => $this ->_idEquipe,
            'nom' => $this ->_nomEquipe,
            'pays' => $this ->_paysEquipe
        ];
    }

}