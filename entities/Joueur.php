<?php

class Joueur implements JsonSerializable
{
    //variables locales
    private $_idJoueur;
    private $_nomJoueur;
    private $_prenomJoueur;
    private $_nationaliteJoueur;
    private $_idEquipe;

    //constructeurs
    public function __construct($idJoueur, $nomJoueur, $prenomJoueur, $nationaliteJoueur, $idEquipe){
        $this->_idJoueur =$idJoueur;
        $this->_nomJoueur = $nomJoueur;
        $this->_prenomJoueur = $prenomJoueur;
        $this->_nationaliteJoueur = $nationaliteJoueur;
        $this->_idEquipe = $idEquipe;
    }

    //getteurs
    public function getIdJoueur(){
        return $this->_idJoueur; 
    }
    public function getNomJoueur(){
        return $this->_nomJoueur;
    }
    public function getPrenomJoueur(){
        return $this->_prenomJoueur;
    }
    public function getNationaliteJoueur(){
        return $this->_nationaliteJoueur;
    }
    public function getIdEquipe(){
        return $this->_idEquipe;
    }

    //setteurs
    public function setIdJoueur($idJoueur){
        $this->_idJoueur = $idJoueur;
    }
    public function setNomJoueur($nomJoueur){
        $this->_nomJoueur = $nomJoueur;
    }
    public function setPrenomJoueur($prenomJoueur){
        $this->_prenomJoueur = $prenomJoueur;
    }
    public function setNationaliteJoueur($nationaliteJoueur){
        $this->_nationaliteJoueur = $nationaliteJoueur;
    }
    public function setIdEquipe($idEquipe){
        $this->_idEquipe = $idEquipe;
    }
    
    public function jsonSerialize()
    {
        return [
        'idJoueur' => $this -> _idJoueur,
        'nomJoueur' => $this -> _nomJoueur,
        'prenomJoueur' => $this -> _prenomJoueur,
        'nationaliteJoueur' => $this -> _nationaliteJoueur,
        'idEquipe' => $this -> _idEquipe
        ];
    }


}