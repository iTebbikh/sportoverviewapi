<?php

class Match implements JsonSerializable
{
    //variables locales
    private $_idMatch;
    private $_dateMatch;
    private $_idEquipeDomicileMatch;
    private $_idEquipeExterieurMatch;
    private $_roundMatch;
    private $_idCompetition;

    //constructeurs
    public function __construct($idMatch, $dateMatch, $idEquipeDomicileMatch, $idEquipeExterieurMatch, $roundMatch, $idCompetition){
        $this->_idMatch =$idMatch;
        $this->_dateMatch = $dateMatch;
        $this->_idEquipeDomicileMatch = $idEquipeDomicileMatch;
        $this->_idEquipeExterieurMatch = $idEquipeExterieurMatch;
        $this->_roundMatch = $roundMatch;
        $this->_idCompetition = $idCompetition;
    }

    //getteurs
    public function getIdMatch(){
        return $this->_idMatch; 
    }
    public function getDateMatch(){
        return $this->_dateMatch;
    }
    public function getIdEquipeDomicileMatch(){
        return $this->_idEquipeDomicileMatch;
    }
    public function getIdEquipeExterieurMatch(){
        return $this->_idEquipeExterieurMatch;
    }
    public function getRoundMatch(){
        return $this->_roundMatch;
    }
    public function getIdCompetition(){
        return $this->_idCompetition; 
    }

    //setteurs
    public function setIdMatch($idMatch){
        $this->_idMatch = $idMatch;
    }
    public function setDateMatch($dateMatch){
        $this->_dateMatch = $dateMatch;
    }
    public function setIdEquipeDomicileMatch($idEquipeDomicileMatch){
        $this->_idEquipeDomicileMatch = $idEquipeDomicileMatch;
    }
    public function setIdEquipeExterieurMatch($idEquipeExterieurMatch){
        $this->_idEquipeExterieurMatch = $idEquipeExterieurMatch;
    }
    public function setRoundMatch($roundMatch){
        $this->_roundMatch = $roundMatch;
    }
    public function setIdCompetition($idCompetition){
        $this->_idCompetition = $idCompetition;
    }

    public function jsonSerialize()
    {
        return [
            'idMatch' => $this -> _idMatch,
            'dateMatch' => $this -> _dateMatch,
            'idEquipeDomicileMatch' => $this -> _idEquipeDomicileMatch,
            'idEquipeExterieurMatch' => $this -> _idEquipeExterieurMatch,
            'roundMatch' => $this -> _roundMatch,
            'idCompetition' => $this -> _idCompetition
        ];
    }

}