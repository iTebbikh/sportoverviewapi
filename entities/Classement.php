<?php

class Classement implements JsonSerializable
{
    //variables locales
    private $_idEquipe;
    private $_idCompetition;
    private $_phaseCompetition;
    private $_matchJoueEquipe;
    private $_matchGagneEquipe;
    private $_matchNulEquipe;
    private $_matchPerduEquipe;
    private $_butMarqueEquipe;
    private $_butEncaisseEquipe;
    private $_differenceButEquipe;
    private $_pointEquipe;

    //constructeurs
    public function __construct($idEquipe, $idCompetition, $phaseCompetition, $matchJoueEquipe, $matchGagneEquipe, $matchNulEquipe, $matchPerduEquipe, $butMarqueEquipe, $butEncaisseEquipe, $differenceButEquipe, $pointEquipe){
        $this->_idEquipe =$idEquipe;
        $this->_idCompetition = $idCompetition;
        $this->_phaseCompetition = $phaseCompetition;
        $this->_matchJoueEquipe = $matchJoueEquipe;
        $this->_matchGagneEquipe =$matchGagneEquipe;
        $this->_matchNulEquipe = $matchNulEquipe;
        $this->_matchPerduEquipe = $matchPerduEquipe;
        $this->_butMarqueEquipe =$butMarqueEquipe;
        $this->_butEncaisseEquipe = $butEncaisseEquipe;
        $this->_differenceButEquipe = $differenceButEquipe;
        $this->_pointEquipe = $pointEquipe;
    }

    //getteurs
    public function getIdEquipe(){
        return $this->_idEquipe; 
    }
    public function getIdCompetition(){
        return $this->_idCompetition;
    }
    public function getPhaseCompetition(){
        return $this->_phaseCompetition;
    }
    public function getMatchJoueEquipe(){
        return $this->_matchJoueEquipe;
    }
    public function getMatchGagneEquipe(){
        return $this->_matchGagneEquipe; 
    }
    public function getMatchNulEquipe(){
        return $this->_matchNulEquipe;
    }
    public function getMatchPerduEquipe(){
        return $this->_matchPerduEquipe;
    }
    public function getButMarqueEquipe(){
        return $this->_butMarqueEquipe; 
    }
    public function getButEncaisseEquipe(){
        return $this->_butEncaisseEquipe;
    }
    public function getDifferenceButEquipe(){
        return $this->_differenceButEquipe;
    }
    public function getPointEquipe(){
        return $this->_pointEquipe;
    }

    //setteurs
    public function setIdEquipe($idEquipe){
        $this->_idEquipe = $idEquipe;
    }
    public function setIdCompetition($idCompetition){
        $this->_idCompetition = $idCompetition;
    }
    public function setPhaseCompetition($phaseCompetition){
        $this->_phaseCompetition = $phaseCompetition;
    }
    public function setMatchJoueEquipe($matchJoueEquipe){
        $this->_matchJoueEquipe = $matchJoueEquipe;
    }
    public function setMatchGagneEquipe($matchGagneEquipe){
        $this->_matchGagneEquipe = $matchGagneEquipe;
    }
    public function setMatchNulEquipe($matchNulEquipe){
        $this->_matchNulEquipe = $matchNulEquipe;
    }
    public function setMatchPerduEquipe($matchPerduEquipe){
        $this->_matchPerduEquipe = $matchPerduEquipe;
    }
    public function setButMarqueEquipe($butMarqueEquipe){
        $this->_butMarqueEquipe = $butMarqueEquipe;
    }
    public function setButEncaisseEquipe($butEncaisseEquipe){
        $this->_butEncaisseEquipe = $butEncaisseEquipe;
    }
    public function setDifferenceButEquipe($differenceButEquipe){
        $this->_differenceButEquipe = $differenceButEquipe;
    }
    public function setPointEquipe($pointEquipe){
        $this->_pointEquipe = $pointEquipe;
    }

    public function jsonSerialize()
    {
        return [

            'idEquipe' => $this ->_idEquipe,
            'idCompetition' => $this -> _idCompetition,
            'phaseCompetition' => $this -> _phaseCompetition,
            'matchJoueEquipe' => $this -> _matchJoueEquipe,
            'matchGagneEquipe' => $this -> _matchGagneEquipe,
            'matchNulEquipe' => $this -> _matchNulEquipe,
            'matchPerduEquipe' => $this -> _matchPerduEquipe,
            'butMarqueEquipe' => $this -> _butMarqueEquipe,
            'butEncaisseEquipe' => $this -> _butEncaisseEquipe,
            'differenceButEquipe' => $this -> _differenceButEquipe,
            'pointEquipe' => $this -> _pointEquipe
        ];
    }

}