<?php

class StatsJoueur implements JsonSerializable
{
    //variables locales
    private $_matchJoueJoueur;
    private $_butMarqueJoueur;
    private $_passeDecisiveJoueur;
    private $_idJoueur;
    private $_idCompetition;

    //constructeurs
    public function __construct($matchJoueJoueur, $butMarqueJoueur, $passeDecisiveJoueur, $idEquipeExterieurMatch, $idCompetition){
        $this->_matchJoueJoueur =$matchJoueJoueur;
        $this->_butMarqueJoueur = $butMarqueJoueur;
        $this->_passeDecisiveJoueur = $passeDecisiveJoueur;
        $this->_idJoueur = $idEquipeExterieurMatch;
        $this->_idCompetition = $idCompetition;
    }

    //getteurs
    public function getMatchJoueJoueur(){
        return $this->_matchJoueJoueur; 
    }
    public function getButMarqueJoueur(){
        return $this->_butMarqueJoueur;
    }
    public function getPasseDecisiveJoueur(){
        return $this->_passeDecisiveJoueur;
    }
    public function getIdJoueur(){
        return $this->_idJoueur; 
    }
    public function getIdCompetition(){
        return $this->_idCompetition; 
    }

    //setteurs
    public function setMatchJoueJoueur($matchJoueJoueur){
        $this->_matchJoueJoueur = $matchJoueJoueur;
    }
    public function setButMarqueJoueur($butMarqueJoueur){
        $this->_butMarqueJoueur = $butMarqueJoueur;
    }
    public function setPasseDecisiveJoueur($passeDecisiveJoueur){
        $this->_passeDecisiveJoueur = $passeDecisiveJoueur;
    }
    public function setIdJoueur($idJoueur){
        $this->_idJoueur = $idJoueur;
    }
    public function setIdCompetition($idCompetition){
        $this->_idCompetition = $idCompetition;
    }

    public function jsonSerialize()
    {
        return [
            'matchJoueJoueur' => $this -> _matchJoueJoueur,
            'butMarqueJoueur' => $this -> _butMarqueJoueur,
            'passeDecisiveJoueur' => $this -> _passeDecisiveJoueur,
            'idJoueur' => $this -> _idJoueur,
            'idCompetition' => $this -> _idCompetition
        ];
    }
}