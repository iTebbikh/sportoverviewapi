<?php

class Competition implements JsonSerializable
{
    //variables locales
    private $_idCompetition;
    private $_nomCompetition;
    private $_saisonCompetition;
    private $_paysCompetition;

    //constructeurs
    public function __construct($idCompetition, $nomCompetition, $saisonCompetition, $paysCompetition){
        $this->_idCompetition =$idCompetition;
        $this->_nomCompetition = $nomCompetition;
        $this->_saisonCompetition = $saisonCompetition;
        $this->_paysCompetition = $paysCompetition;
    }

    //getteurs
    public function getIdCompetition(){
        return $this->_idCompetition; 
    }
    public function getNomCompetition(){
        return $this->_nomCompetition;
    }
    public function getSaisonCompetition(){
        return $this->_saisonCompetition;
    }
    public function getPaysCompetition(){
        return $this->_paysCompetition;
    }

    //setteurs
    public function setIdCompetition($idCompetition){
        $this->_idCompetition = $idCompetition;
    }
    public function setNomCompetition($nomCompetition){
        $this->_nomCompetition = $nomCompetition;
    }
    public function setSaisonCompetition($saisonCompetition){
        $this->_saisonCompetition = $saisonCompetition;
    }
    public function setPaysCompetition($paysCompetition){
        $this->_paysCompetition = $paysCompetition;
    }

    public function jsonSerialize()
    {
        return [
        'idCompetition' => $this -> _idCompetition,
        'nomCompetition' => $this -> _nomCompetition,
        'saisonCompetition' => $this -> _saisonCompetition,
        'paysCompetition'=> $this -> _paysCompetition
        ];
    } 

    


}