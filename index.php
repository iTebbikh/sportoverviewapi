<?php
/**
 * Point d'entrée unique de notre application
 * C'est ce script PHP qui décide quel autre script PHP sera appelé en fonction
 */

require_once('./configuration/config.php');

// Ici la liste de vos APIs et de leurs scripts associés
$apis = [
    "allEquipe" => "controllers/apiAllEquipe.php",
    "getEquipeId/(\d+)" => "controllers/apiGetEquipeId.php",

    "allCompetition" => "controllers/apiAllCompetition.php",
    "getCompetitionId/(\d+)" => "controllers/apiGetCompetitionId.php",

    "getClassementId/(\d+)" => "controllers/apiGetClassementId.php",

    "allJoueur" => "controllers/apiAllJoueur.php",
    "getJoueurId/(\d+)" => "controllers/apiGetJoueurId.php",

    "allMatch" => "controllers/apiAllMatch.php",
    "getMatchId/(\d+)" => "controllers/apiGetMatchId.php",
    "getMatchIdEquipeDomicile/(\d+)" => "controllers/apiGetMatchIdEquipeDomicile.php",
    "getMatchIdEquipeExterieur/(\d+)" => "controllers/apiGetMatchIdExterieur.php",
    "getMatchRound/(\d+)" => "controllers/apiGetMatchRound.php",
    "getMatchIdCompetition/(\d+)" => "controllers/apiGetMatchIdCompetition.php",
    "getLastMatchIdCompetition/(\d+)" => "controllers/apiGetLastMatchIdCompetition.php",

    "allScore" => "controllers/apiAllScore.php",
    "getScoreId/(\d+)" => "controllers/apiGetScoreId.php",


    
    "totalBut/(\d+)" => "controllers/apiTotalBut.php",

    "allStatsJoueur" => "controllers/apiAllStatsJoueur.php",
    "getStatsJoueurId/(\d+)" => "controllers/apiStatsJoueurId.php"

];


// En l'absence d'API clairement demandée : 404
if(empty($_GET['url'])) {
    displayAccueil();
} else {
    $include_script = "";
    $matches = [];
    foreach ($apis as $url_model => $script) {
        // On recherche parmis les modèles lequel correspond à l'URL fournie
        if (preg_match('%^'.$url_model.'(/.*)?$%i', $_GET['url'], $matches) ) {
            $include_script = $script;
            $params = $matches;
            array_shift($params); // retirer le premier élément, inutile
        }
    }
    if (empty($include_script)) {
        displayAccueil();
    } else {
        $_GET['api_params'] = $params;
        require_once $include_script;
    }
}

function displayAccueil() {
    header('Location: ?url=allEquipe');
}
