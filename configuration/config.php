<?php
 
const DEBUG = true; 

//Accès à la base de donnée phpMyAdmin
const BD_HOST = 'localhost';
const BD_DBNAME = 'SportOverview';
const BD_USER = 'postgres';
const BD_PWD = 'root';

//dossiers racines du site
define('PATH_CONTROLLERS', './controllers/c_');
define('PATH_MODELS', './models/');
define('PATH_ENTITY', './entities/');


?>